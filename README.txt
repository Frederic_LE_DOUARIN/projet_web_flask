-- création du dossier contenant le projet et le venv
mkdir dossier
cd dossier

-- clone du projet
git clone https://gitlab.com/WassimOulhadj/projet_web_flask.git

-- création du venv
windows : python3 -m venv venv
linux : virtualenv -p python3 venv

-- activation du venv
windows: venv\Scripts\activate
linux: source venv/bin/activate

-- installations des logiciels utiles dans le venv
cd Projet_Web_Flask
pip install -r requirements.txt

-- créer la bd et insert des données
flask loaddb
flask syncdb

-- lancer l'application
flask run
Sur un navigateur internet : 
localhost:5000/

Pour désactiver le venv : deactivate