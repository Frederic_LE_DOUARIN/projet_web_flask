from app import db 
from flask_login import UserMixin
from sqlalchemy import *
from flask_sqlalchemy import *


class Utilisateur(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    pseudo = db.Column(db.String(30), primary_key=True)
    mail = db.Column(db.String(60))
    nom = db.Column(db.String(30))
    prenom = db.Column(db.String(30))
    mdp = db.Column(db.String(30))
    sexe = db.Column(db.String(30))
    dateNaissance = db.Column(db.String(20))

    def get_pseudo(self):
        return self.pseudo

    def __repr__(self):
        return self.pseudo

    def get_mail(self):
        return str(self.mail)

class Artist(db.Model):
    id_artist = db.Column(db.Integer, primary_key=True)
    nomPrenom = db.Column(db.String(80))

    def get_nomPrenom(self):
        return self.nomPrenom

    def __repr__(self):
        return "Playlist %s de %s" % (self.nomPrenom, self.id_artist)

association_genre_album = Table(
    "association_genre_album", 
    db.Model.metadata, 
    db.Column("genre_id", db.ForeignKey("Genre.id"), primary_key=True),
    db.Column("album_id", db.ForeignKey("Album.id"), primary_key=True)
)

association_playlist_album = Table(
    "association_playlist_album", 
    db.Model.metadata, 
    db.Column("album", db.ForeignKey("Album.id"), primary_key=True),
    db.Column("playlist", db.ForeignKey("Playlist.id"), primary_key=True)
)

class Album(db.Model):
    __tablename__ = "Album"
    id = db.Column(db.Integer, primary_key=True)
    auteur_id = db.Column(db.Integer, db.ForeignKey("artist.id_artist"))
    titre = db.Column(db.String(100))
    dateDeSortie = db.Column(db.Integer)
    urlImage = db.Column(db.String(300))
    genres = db.relationship(
        "Genre",
        secondary=association_genre_album,
        back_populates="albums")
    playlist = db.relationship(
        "Playlist",
        secondary=association_playlist_album,
        back_populates="album")


    def __repr__(self):
        return "%s , %s, %s, %s, %s, %s" % (self.auteur_id, self.titre, self.dateDeSortie, self.urlImage, self.genres, self.playlist)


class Playlist(db.Model):
    __tablename__ = "Playlist"
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(100))
    album = db.relationship(
        "Album",
        secondary=association_playlist_album,
        back_populates="playlist")
    # urlImage = db.Column(db.String(300))
    # liste_album = db.Column(ARRAY(String))
    # utilisateur_id = db.Column(db.Integer, db.ForeignKey("utilisateur.id"))

    def __repr__(self):
        return "%sPlaylist %s %s" % (self.id, self.nom, self.album)

class Genre(db.Model):
    __tablename__ = "Genre"
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(100))
    albums = db.relationship(
        "Album",
        secondary=association_genre_album,
        back_populates="genres")

    def __repr__(self):
        return "%s" % (self.nom)

# class contenir(db.Model):
#     idPlaylist = db.Column(db.Integer, db.ForeignKey("playlist.id"), primary_key=True)
#     idAlbum = db.Column(db.Integer, db.ForeignKey("album.id"), primary_key=True)
    

class note(db.Model):
    id_note = db.Column(db.Integer, primary_key = True)
    id_album = db.Column(db.Integer, db.ForeignKey("Album.id"))
    id_utilisateur = db.Column(db.Integer, db.ForeignKey("utilisateur.id"))
    note_album = db.Column(db.Float(1,1))


def recuperer_pseudo(pseudo):
   return Utilisateur.query.filter_by(pseudo=pseudo).first()

def get_pseudo_mdp(pseudo,mdp):
    users = Utilisateur.query.order_by(Utilisateur.id).all()
    print(users)
    for user in users:
        if user.pseudo == pseudo and user.mdp == mdp:
            return True
    return False

def get_auteur_by_id(id):
    user = Utilisateur.query.filter_by(id=id).first()
    return str(user.auteur_id)


def get_titre_by_id(id):
    user = Utilisateur.query.filter_by(id=id).first()
    return str(user.titre)

def get_date_by_id(id):
    user = Utilisateur.query.filter_by(id=id).first()
    return str(user.dateDeSortie)

def get_image_by_id(id):
    user = Utilisateur.query.filter_by(id=id).first()
    return str(user.urlImage)


def recuperer_id(id):
   return Utilisateur.query.filter_by(id=id).first()

def get_artist_by_id(id):
    return Artist.query.filter_by(id_artist=id).first()

def get_album_by_id(id):
    return Album.query.filter_by(id=id).first()

def get_pseudos_users():
    users = Utilisateur.query.order_by(Utilisateur.id).all()
    liste_users = []
    for user in users:
        liste_users.append(str(user))
    return liste_users

def get_mail_users():
    users = Utilisateur.query.order_by(Utilisateur.id).all()
    liste_users = []
    for user in users:
        liste_users.append(user.mail)
    return liste_users

def get_max_new_id():
    qry = db.session.query(func.max(Artist.id_artist))
    res = qry.one()
    maxi = res[0]
    if maxi is None:
        id_first = 0
    else:
        id_first = maxi + 1
    return id_first

def get_artist():
    Artiste = Artist.query.order_by(Artist.id_artist).all()
    liste_artiste = []
    for art in Artiste:
        liste_artiste.append(art.nomPrenom)
    return liste_artiste

def get_genre():
    album = Album.query.order_by(Album.id).all()
    liste_album = []
    for al in album:
        liste_album.append(al)
    liste=[]
    for genre in liste_album:
        liste.append(genre.genre)
    return liste

def get_Albums_v1():
    album = Album.query.order_by(Album.id).all()
    liste_album = []
    for al in album:
        liste_album.append(al)
    liste=[]
    for titre in liste_album:
        liste.append(titre.titre)
    return liste

def get_album_by_id(id):
    return Album.query.filter_by(id=id).first()

def get_id_by_artiste(artiste):
    Artiste = Artist.query.order_by(Artist.id_artist).all()
    liste_artiste = []
    for art in Artiste:
        if Artist.nomPrenom==artiste:
            liste_artiste.append(art)
    return liste_artiste
def get_playlist_by_id(id):
    return Playlist.query.filter(Playlist.id==id).first()

def get_genre_by_nom(nom):
    g = Genre.query.filter_by(nom=nom).first()
    return g


def get_max_new_id_playlist():
    qry = db.session.query(func.max(Playlist.id))
    res = qry.one()
    maxi = res[0]
    if maxi is None:
        id_first = 0
    else:
        id_first = maxi + 1
    return id_first

def get_max_new_id_genre():
    qry = db.session.query(func.max(Genre.id))
    res = qry.one()
    maxi = res[0]
    if maxi is None:
        id_first = 0
    else:
        id_first = maxi + 1
    return id_first

def get_playlist():
    playlist = Playlist.query.order_by(Playlist.id).all()
    return playlist

def get_playlist_by_name(name):
    playlist = Playlist.query.filter(Playlist.nom==name).first()
    return playlist

def get_playlist_name(nom):
    playlist = Playlist.query.filter(Playlist.nom==nom).first() 
    print(playlist)
    if playlist==[]:
        return True
    else:
        return False


def get_listes_genres_in_album(recherche):
    res = []
    album = Album.query.order_by(Album.id).all()
    for elem in album:
        for g in elem.genres:
            if g.nom==recherche:
                res.append(album)
    return res            


def get_all_genres():
    return Genre.query.order_by(Genre.nom).all()

def tri_croissant_album():
    albums = Album.query.order_by(Album.dateDeSortie.asc()).all()
    return albums

def get_decroissant_album():
    albums = Album.query.order_by(Album.dateDeSortie.asc()).all()
    return albums





