from flask import Flask
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
import os.path

app = Flask(__name__)
Bootstrap(app)
app.config['BOOTSTRAP_SERVE_LOCAL'] = True

def mkpath(p):
    return os.path.normpath(
        os.path.join(
        os.path.dirname(__file__),
        p))

app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('./skypop.db'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config["SECRET_KEY"]="skypop"

db = SQLAlchemy(app)

import commands
from views import *

